WITH learner_activity_with_due_date_in_term AS (
  SELECT
    la.key
    , la.learner_activity_id
    , la.points_possible
    , cs.instruction_begin_date
    , la.due_date
    , CASE WHEN la.due_date IS NULL THEN TRUE ELSE FALSE END as null_due_date
    , CASE WHEN la.due_date IS NOT NULL THEN TRUE ELSE FALSE END as has_due_date
    , CASE WHEN cs.instruction_begin_date IS NULL THEN TRUE ELSE FALSE END as null_instruction_begin_date
    , CASE WHEN cs.instruction_begin_date IS NOT NULL THEN TRUE ELSE FALSE END as has_instruction_begin_date
    , CASE WHEN la.due_date IS NULL OR cs.instruction_begin_date IS NULL THEN TRUE ELSE FALSE END as null_due_date_or_begin_instruction_date
    , CASE WHEN la.due_date <= cs.instruction_begin_date THEN TRUE ELSE FALSE END as before_course_starts
    , CASE WHEN la.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 7 DAY) THEN TRUE ELSE FALSE END as by_week_1
    , CASE WHEN la.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 14 DAY) THEN TRUE ELSE FALSE END as by_week_2
    , CASE WHEN la.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 21 DAY) THEN TRUE ELSE FALSE END as by_week_3
    , CASE WHEN la.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 28 DAY) THEN TRUE ELSE FALSE END as by_week_4
    , CASE WHEN la.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 35 DAY) THEN TRUE ELSE FALSE END as by_week_5
    , CASE WHEN la.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 42 DAY) THEN TRUE ELSE FALSE END as by_week_6
    , CASE WHEN la.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 49 DAY) THEN TRUE ELSE FALSE END as by_week_7
    , CASE WHEN la.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 56 DAY) THEN TRUE ELSE FALSE END as by_week_8
    , CASE WHEN la.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 63 DAY) THEN TRUE ELSE FALSE END as by_week_9
    , CASE WHEN la.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 70 DAY) THEN TRUE ELSE FALSE END as by_week_10
    , CASE WHEN la.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 77 DAY) THEN TRUE ELSE FALSE END as by_week_11
    , CASE WHEN la.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 84 DAY) THEN TRUE ELSE FALSE END as by_week_12
    , CASE WHEN la.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 91 DAY) THEN TRUE ELSE FALSE END as by_week_13
    , CASE WHEN la.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 98 DAY) THEN TRUE ELSE FALSE END as by_week_14
    , CASE WHEN la.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 105 DAY) THEN TRUE ELSE FALSE END as by_week_15
    , CASE WHEN la.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 112 DAY) THEN TRUE ELSE FALSE END as by_week_16
    , CASE WHEN la.due_date >  DATETIME_ADD(cs.instruction_begin_date, INTERVAL 112 DAY) THEN TRUE ELSE FALSE END as beyond_week_16

  FROM
    cgr_disaggregated.learner_activity la

  LEFT JOIN
    cgr_disaggregated.course_section AS cs ON la.key = cs.key
)

SELECT
  SUM(CASE WHEN z.null_due_date IS TRUE THEN 1 ELSE 0 END) null_due_date
  , SUM(CASE WHEN z.has_due_date IS TRUE THEN 1 ELSE 0 END) has_due_date
  , SUM(CASE WHEN z.null_instruction_begin_date IS TRUE THEN 1 ELSE 0 END) null_instruction_begin_date
  , SUM(CASE WHEN z.has_instruction_begin_date IS TRUE THEN 1 ELSE 0 END) has_instruction_begin_date
  , SUM(CASE WHEN z.null_due_date_or_begin_instruction_date IS TRUE THEN 1 ELSE 0 END) null_due_date_or_begin_instruction_date
  , SUM(CASE WHEN z.before_course_starts IS TRUE THEN 1 ELSE 0 END) before_course_starts
  , SUM(CASE WHEN z.by_week_1 IS TRUE THEN 1 ELSE 0 END) by_week_1
  , SUM(CASE WHEN z.by_week_2 IS TRUE THEN 1 ELSE 0 END) by_week_2
  , SUM(CASE WHEN z.by_week_3 IS TRUE THEN 1 ELSE 0 END) by_week_3
  , SUM(CASE WHEN z.by_week_4 IS TRUE THEN 1 ELSE 0 END) by_week_4
  , SUM(CASE WHEN z.by_week_5 IS TRUE THEN 1 ELSE 0 END) by_week_5
  , SUM(CASE WHEN z.by_week_6 IS TRUE THEN 1 ELSE 0 END) by_week_6
  , SUM(CASE WHEN z.by_week_7 IS TRUE THEN 1 ELSE 0 END) by_week_7
  , SUM(CASE WHEN z.by_week_8 IS TRUE THEN 1 ELSE 0 END) by_week_8
  , SUM(CASE WHEN z.by_week_9 IS TRUE THEN 1 ELSE 0 END) by_week_9
  , SUM(CASE WHEN z.by_week_10 IS TRUE THEN 1 ELSE 0 END) by_week_10
  , SUM(CASE WHEN z.by_week_11 IS TRUE THEN 1 ELSE 0 END) by_week_11
  , SUM(CASE WHEN z.by_week_12 IS TRUE THEN 1 ELSE 0 END) by_week_12
  , SUM(CASE WHEN z.by_week_13 IS TRUE THEN 1 ELSE 0 END) by_week_13
  , SUM(CASE WHEN z.by_week_14 IS TRUE THEN 1 ELSE 0 END) by_week_14
  , SUM(CASE WHEN z.by_week_15 IS TRUE THEN 1 ELSE 0 END) by_week_15
  , SUM(CASE WHEN z.by_week_16 IS TRUE THEN 1 ELSE 0 END) by_week_16
  , SUM(CASE WHEN z.beyond_week_16 IS TRUE THEN 1 ELSE 0 END) beyond_week_16
  , COUNT(1) total_learner_activities

FROM
  learner_activity_with_due_date_in_term AS z
