WITH learner_activity_results_with_due_date_in_term AS (
  SELECT
    lar.person_id
    , lar.key
    , lar.learner_activity_id
    , lar.points_possible
    , lar.published_score
    , cs.instruction_begin_date
    , lar.response_date
    , CASE WHEN la.due_date IS NULL THEN TRUE ELSE FALSE END AS null_due_date
    , CASE WHEN la.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 7 DAY) THEN TRUE ELSE FALSE END AS by_week_1
    , CASE WHEN la.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 14 DAY) THEN TRUE ELSE FALSE END AS by_week_2
    , CASE WHEN la.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 21 DAY) THEN TRUE ELSE FALSE END AS by_week_3
    , CASE WHEN la.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 28 DAY) THEN TRUE ELSE FALSE END AS by_week_4
    , CASE WHEN la.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 35 DAY) THEN TRUE ELSE FALSE END AS by_week_5
    , CASE WHEN la.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 42 DAY) THEN TRUE ELSE FALSE END AS by_week_6
    , CASE WHEN la.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 49 DAY) THEN TRUE ELSE FALSE END AS by_week_7
    , CASE WHEN la.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 56 DAY) THEN TRUE ELSE FALSE END AS by_week_8
    , CASE WHEN la.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 63 DAY) THEN TRUE ELSE FALSE END AS by_week_9
    , CASE WHEN la.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 70 DAY) THEN TRUE ELSE FALSE END AS by_week_10
    , CASE WHEN la.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 77 DAY) THEN TRUE ELSE FALSE END AS by_week_11
    , CASE WHEN la.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 84 DAY) THEN TRUE ELSE FALSE END AS by_week_12
    , CASE WHEN la.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 91 DAY) THEN TRUE ELSE FALSE END AS by_week_13
    , CASE WHEN la.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 98 DAY) THEN TRUE ELSE FALSE END AS by_week_14
    , CASE WHEN la.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 105 DAY) THEN TRUE ELSE FALSE END AS by_week_15
    , CASE WHEN la.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 112 DAY) THEN TRUE ELSE FALSE END AS by_week_16
    , CASE WHEN la.due_date >  DATETIME_ADD(cs.instruction_begin_date, INTERVAL 112 DAY) THEN TRUE ELSE FALSE END AS beyond_week_16

  FROM
    cgr_disaggregated.learner_activity_result lar

  INNER JOIN
    cgr_disaggregated.course_section AS cs ON lar.key = cs.key

  INNER JOIN
    cgr_disaggregated.learner_activity la ON lar.learner_activity_id = la.learner_activity_lms_int_id
)

SELECT
  lar.person_id AS person_id
  , lar.key AS key

  /* Total scores, by week, cumulative */
  , SUM(CASE WHEN lar.by_week_1 IS TRUE THEN lar.published_score ELSE 0 END) AS total_score_by_week_1
  , SUM(CASE WHEN lar.by_week_2 IS TRUE THEN lar.published_score ELSE 0 END) AS total_score_by_week_2
  , SUM(CASE WHEN lar.by_week_3 IS TRUE THEN lar.published_score ELSE 0 END) AS total_score_by_week_3
  , SUM(CASE WHEN lar.by_week_4 IS TRUE THEN lar.published_score ELSE 0 END) AS total_score_by_week_4
  , SUM(CASE WHEN lar.by_week_5 IS TRUE THEN lar.published_score ELSE 0 END) AS total_score_by_week_5
  , SUM(CASE WHEN lar.by_week_6 IS TRUE THEN lar.published_score ELSE 0 END) AS total_score_by_week_6
  , SUM(CASE WHEN lar.by_week_7 IS TRUE THEN lar.published_score ELSE 0 END) AS total_score_by_week_7
  , SUM(CASE WHEN lar.by_week_8 IS TRUE THEN lar.published_score ELSE 0 END) AS total_score_by_week_8
  , SUM(CASE WHEN lar.by_week_9 IS TRUE THEN lar.published_score ELSE 0 END) AS total_score_by_week_9
  , SUM(CASE WHEN lar.by_week_10 IS TRUE THEN lar.published_score ELSE 0 END) AS total_score_by_week_10
  , SUM(CASE WHEN lar.by_week_11 IS TRUE THEN lar.published_score ELSE 0 END) AS total_score_by_week_11
  , SUM(CASE WHEN lar.by_week_12 IS TRUE THEN lar.published_score ELSE 0 END) AS total_score_by_week_12
  , SUM(CASE WHEN lar.by_week_13 IS TRUE THEN lar.published_score ELSE 0 END) AS total_score_by_week_13
  , SUM(CASE WHEN lar.by_week_14 IS TRUE THEN lar.published_score ELSE 0 END) AS total_score_by_week_14
  , SUM(CASE WHEN lar.by_week_15 IS TRUE THEN lar.published_score ELSE 0 END) AS total_score_by_week_15
  , SUM(CASE WHEN lar.by_week_16 IS TRUE THEN lar.published_score ELSE 0 END) AS total_score_by_week_16
  , SUM(CASE WHEN lar.beyond_week_16 IS TRUE THEN lar.published_score ELSE 0 END) AS total_score_beyond_week_16
  , SUM(lar.published_score) total_score_for_course

  /* Total points possible, by week, cumulative */
  , SUM(CASE WHEN lar.by_week_1 IS TRUE THEN lar.points_possible ELSE 0 END) AS total_points_possible_by_week_1
  , SUM(CASE WHEN lar.by_week_2 IS TRUE THEN lar.points_possible ELSE 0 END) AS total_points_possible_by_week_2
  , SUM(CASE WHEN lar.by_week_3 IS TRUE THEN lar.points_possible ELSE 0 END) AS total_points_possible_by_week_3
  , SUM(CASE WHEN lar.by_week_4 IS TRUE THEN lar.points_possible ELSE 0 END) AS total_points_possible_by_week_4
  , SUM(CASE WHEN lar.by_week_5 IS TRUE THEN lar.points_possible ELSE 0 END) AS total_points_possible_by_week_5
  , SUM(CASE WHEN lar.by_week_6 IS TRUE THEN lar.points_possible ELSE 0 END) AS total_points_possible_by_week_6
  , SUM(CASE WHEN lar.by_week_7 IS TRUE THEN lar.points_possible ELSE 0 END) AS total_points_possible_by_week_7
  , SUM(CASE WHEN lar.by_week_8 IS TRUE THEN lar.points_possible ELSE 0 END) AS total_points_possible_by_week_8
  , SUM(CASE WHEN lar.by_week_9 IS TRUE THEN lar.points_possible ELSE 0 END) AS total_points_possible_by_week_9
  , SUM(CASE WHEN lar.by_week_10 IS TRUE THEN lar.points_possible ELSE 0 END) AS total_points_possible_by_week_10
  , SUM(CASE WHEN lar.by_week_11 IS TRUE THEN lar.points_possible ELSE 0 END) AS total_points_possible_by_week_11
  , SUM(CASE WHEN lar.by_week_12 IS TRUE THEN lar.points_possible ELSE 0 END) AS total_points_possible_by_week_12
  , SUM(CASE WHEN lar.by_week_13 IS TRUE THEN lar.points_possible ELSE 0 END) AS total_points_possible_by_week_13
  , SUM(CASE WHEN lar.by_week_14 IS TRUE THEN lar.points_possible ELSE 0 END) AS total_points_possible_by_week_14
  , SUM(CASE WHEN lar.by_week_15 IS TRUE THEN lar.points_possible ELSE 0 END) AS total_points_possible_by_week_15
  , SUM(CASE WHEN lar.by_week_16 IS TRUE THEN lar.points_possible ELSE 0 END) AS total_points_possible_by_week_16
  , SUM(CASE WHEN lar.beyond_week_16 IS TRUE THEN lar.points_possible ELSE 0 END) AS total_points_possible_beyond_week_16
  , SUM(lar.points_possible) total_points_possible_for_course

  /* Average score */
  , CASE
    WHEN
      SUM(CASE WHEN lar.by_week_1 IS TRUE THEN lar.points_possible ELSE 0 END) IS NOT NULL AND
      SUM(CASE WHEN lar.by_week_1 IS TRUE THEN lar.points_possible ELSE 0 END) > 0
    THEN
      SUM(CASE WHEN lar.by_week_1 IS TRUE THEN lar.published_score ELSE 0 END) /
      SUM(CASE WHEN lar.by_week_1 IS TRUE THEN lar.points_possible ELSE 0 END)
    ELSE NULL
    END AS avg_score_by_week_1

  , CASE
    WHEN
      SUM(CASE WHEN lar.by_week_2 IS TRUE THEN lar.points_possible ELSE 0 END) IS NOT NULL AND
      SUM(CASE WHEN lar.by_week_2 IS TRUE THEN lar.points_possible ELSE 0 END) > 0
    THEN
      SUM(CASE WHEN lar.by_week_2 IS TRUE THEN lar.published_score ELSE 0 END) /
      SUM(CASE WHEN lar.by_week_2 IS TRUE THEN lar.points_possible ELSE 0 END)
    ELSE NULL
    END AS avg_score_by_week_2

  , CASE
    WHEN
      SUM(CASE WHEN lar.by_week_3 IS TRUE THEN lar.points_possible ELSE 0 END) IS NOT NULL AND
      SUM(CASE WHEN lar.by_week_3 IS TRUE THEN lar.points_possible ELSE 0 END) > 0
    THEN
      SUM(CASE WHEN lar.by_week_3 IS TRUE THEN lar.published_score ELSE 0 END) /
      SUM(CASE WHEN lar.by_week_3 IS TRUE THEN lar.points_possible ELSE 0 END)
    ELSE NULL
    END AS avg_score_by_week_3

  , CASE
    WHEN
      SUM(CASE WHEN lar.by_week_4 IS TRUE THEN lar.points_possible ELSE 0 END) IS NOT NULL AND
      SUM(CASE WHEN lar.by_week_4 IS TRUE THEN lar.points_possible ELSE 0 END) > 0
    THEN
      SUM(CASE WHEN lar.by_week_4 IS TRUE THEN lar.published_score ELSE 0 END) /
      SUM(CASE WHEN lar.by_week_4 IS TRUE THEN lar.points_possible ELSE 0 END)
    ELSE NULL
    END AS avg_score_by_week_4

  , CASE
    WHEN
      SUM(CASE WHEN lar.by_week_5 IS TRUE THEN lar.points_possible ELSE 0 END) IS NOT NULL AND
      SUM(CASE WHEN lar.by_week_5 IS TRUE THEN lar.points_possible ELSE 0 END) > 0
    THEN
      SUM(CASE WHEN lar.by_week_5 IS TRUE THEN lar.published_score ELSE 0 END) /
      SUM(CASE WHEN lar.by_week_5 IS TRUE THEN lar.points_possible ELSE 0 END)
    ELSE NULL
    END AS avg_score_by_week_5

  , CASE
    WHEN
      SUM(CASE WHEN lar.by_week_6 IS TRUE THEN lar.points_possible ELSE 0 END) IS NOT NULL AND
      SUM(CASE WHEN lar.by_week_6 IS TRUE THEN lar.points_possible ELSE 0 END) > 0
    THEN
      SUM(CASE WHEN lar.by_week_6 IS TRUE THEN lar.published_score ELSE 0 END) /
      SUM(CASE WHEN lar.by_week_6 IS TRUE THEN lar.points_possible ELSE 0 END)
    ELSE NULL
    END AS avg_score_by_week_6

  , CASE
    WHEN
      SUM(CASE WHEN lar.by_week_7 IS TRUE THEN lar.points_possible ELSE 0 END) IS NOT NULL AND
      SUM(CASE WHEN lar.by_week_7 IS TRUE THEN lar.points_possible ELSE 0 END) > 0
    THEN
      SUM(CASE WHEN lar.by_week_7 IS TRUE THEN lar.published_score ELSE 0 END) /
      SUM(CASE WHEN lar.by_week_7 IS TRUE THEN lar.points_possible ELSE 0 END)
    ELSE NULL
    END AS avg_score_by_week_7

  , CASE
    WHEN
      SUM(CASE WHEN lar.by_week_8 IS TRUE THEN lar.points_possible ELSE 0 END) IS NOT NULL AND
      SUM(CASE WHEN lar.by_week_8 IS TRUE THEN lar.points_possible ELSE 0 END) > 0
    THEN
      SUM(CASE WHEN lar.by_week_8 IS TRUE THEN lar.published_score ELSE 0 END) /
      SUM(CASE WHEN lar.by_week_8 IS TRUE THEN lar.points_possible ELSE 0 END)
    ELSE NULL
    END AS avg_score_by_week_8

  , CASE
    WHEN
      SUM(CASE WHEN lar.by_week_9 IS TRUE THEN lar.points_possible ELSE 0 END) IS NOT NULL AND
      SUM(CASE WHEN lar.by_week_9 IS TRUE THEN lar.points_possible ELSE 0 END) > 0
    THEN
      SUM(CASE WHEN lar.by_week_9 IS TRUE THEN lar.published_score ELSE 0 END) /
      SUM(CASE WHEN lar.by_week_9 IS TRUE THEN lar.points_possible ELSE 0 END)
    ELSE NULL
    END AS avg_score_by_week_9

  , CASE
    WHEN
      SUM(CASE WHEN lar.by_week_10 IS TRUE THEN lar.points_possible ELSE 0 END) IS NOT NULL AND
      SUM(CASE WHEN lar.by_week_10 IS TRUE THEN lar.points_possible ELSE 0 END) > 0
    THEN
      SUM(CASE WHEN lar.by_week_10 IS TRUE THEN lar.published_score ELSE 0 END) /
      SUM(CASE WHEN lar.by_week_10 IS TRUE THEN lar.points_possible ELSE 0 END)
    ELSE NULL
    END AS avg_score_by_week_10

  , CASE
    WHEN
      SUM(CASE WHEN lar.by_week_11 IS TRUE THEN lar.points_possible ELSE 0 END) IS NOT NULL AND
      SUM(CASE WHEN lar.by_week_11 IS TRUE THEN lar.points_possible ELSE 0 END) > 0
    THEN
      SUM(CASE WHEN lar.by_week_11 IS TRUE THEN lar.published_score ELSE 0 END) /
      SUM(CASE WHEN lar.by_week_11 IS TRUE THEN lar.points_possible ELSE 0 END)
    ELSE NULL
    END AS avg_score_by_week_11

  , CASE
    WHEN
      SUM(CASE WHEN lar.by_week_12 IS TRUE THEN lar.points_possible ELSE 0 END) IS NOT NULL AND
      SUM(CASE WHEN lar.by_week_12 IS TRUE THEN lar.points_possible ELSE 0 END) > 0
    THEN
      SUM(CASE WHEN lar.by_week_12 IS TRUE THEN lar.published_score ELSE 0 END) /
      SUM(CASE WHEN lar.by_week_12 IS TRUE THEN lar.points_possible ELSE 0 END)
    ELSE NULL
    END AS avg_score_by_week_12

  , CASE
    WHEN
      SUM(CASE WHEN lar.by_week_13 IS TRUE THEN lar.points_possible ELSE 0 END) IS NOT NULL AND
      SUM(CASE WHEN lar.by_week_13 IS TRUE THEN lar.points_possible ELSE 0 END) > 0
    THEN
      SUM(CASE WHEN lar.by_week_13 IS TRUE THEN lar.published_score ELSE 0 END) /
      SUM(CASE WHEN lar.by_week_13 IS TRUE THEN lar.points_possible ELSE 0 END)
    ELSE NULL
    END AS avg_score_by_week_13

  , CASE
    WHEN
      SUM(CASE WHEN lar.by_week_14 IS TRUE THEN lar.points_possible ELSE 0 END) IS NOT NULL AND
      SUM(CASE WHEN lar.by_week_14 IS TRUE THEN lar.points_possible ELSE 0 END) > 0
    THEN
      SUM(CASE WHEN lar.by_week_14 IS TRUE THEN lar.published_score ELSE 0 END) /
      SUM(CASE WHEN lar.by_week_14 IS TRUE THEN lar.points_possible ELSE 0 END)
    ELSE NULL
    END AS avg_score_by_week_14

  , CASE
    WHEN
      SUM(CASE WHEN lar.by_week_15 IS TRUE THEN lar.points_possible ELSE 0 END) IS NOT NULL AND
      SUM(CASE WHEN lar.by_week_15 IS TRUE THEN lar.points_possible ELSE 0 END) > 0
    THEN
      SUM(CASE WHEN lar.by_week_15 IS TRUE THEN lar.published_score ELSE 0 END) /
      SUM(CASE WHEN lar.by_week_15 IS TRUE THEN lar.points_possible ELSE 0 END)
    ELSE NULL
    END AS avg_score_by_week_15

  , CASE
    WHEN
      SUM(CASE WHEN lar.by_week_16 IS TRUE THEN lar.points_possible ELSE 0 END) IS NOT NULL AND
      SUM(CASE WHEN lar.by_week_16 IS TRUE THEN lar.points_possible ELSE 0 END) > 0
    THEN
      SUM(CASE WHEN lar.by_week_16 IS TRUE THEN lar.published_score ELSE 0 END) /
      SUM(CASE WHEN lar.by_week_16 IS TRUE THEN lar.points_possible ELSE 0 END)
    ELSE NULL
    END AS avg_score_by_week_16

  , CASE
    WHEN
      SUM(CASE WHEN lar.beyond_week_16 IS TRUE THEN lar.points_possible ELSE 0 END) IS NOT NULL AND
      SUM(CASE WHEN lar.beyond_week_16 IS TRUE THEN lar.points_possible ELSE 0 END) > 0
    THEN
      SUM(CASE WHEN lar.beyond_week_16 IS TRUE THEN lar.published_score ELSE 0 END) /
      SUM(CASE WHEN lar.beyond_week_16 IS TRUE THEN lar.points_possible ELSE 0 END)
    ELSE NULL
    END AS avg_score_beyond_week_16

  , CASE
    WHEN
      SUM(lar.points_possible) IS NOT NULL AND SUM(lar.points_possible) > 0
    THEN
      SUM(lar.published_score) / SUM(lar.points_possible)
    ELSE NULL
    END AS avg_score_for_course

FROM
  learner_activity_results_with_due_date_in_term lar

GROUP BY
  lar.person_id
  , lar.key
;
