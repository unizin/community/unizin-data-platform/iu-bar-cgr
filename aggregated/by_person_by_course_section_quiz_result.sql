WITH quiz_results_with_due_date_in_term AS (
  SELECT
    qr.person_id
    , qr.key
    , qr.quiz_id
    , qr.points_possible
    , qr.kept_score
    , cs.instruction_begin_date
    , qr.finished_date
    , CASE WHEN q.due_date IS NULL THEN TRUE ELSE FALSE END AS null_due_date
    , CASE WHEN q.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 7 DAY) THEN TRUE ELSE FALSE END AS by_week_1
    , CASE WHEN q.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 14 DAY) THEN TRUE ELSE FALSE END AS by_week_2
    , CASE WHEN q.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 21 DAY) THEN TRUE ELSE FALSE END AS by_week_3
    , CASE WHEN q.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 28 DAY) THEN TRUE ELSE FALSE END AS by_week_4
    , CASE WHEN q.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 35 DAY) THEN TRUE ELSE FALSE END AS by_week_5
    , CASE WHEN q.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 42 DAY) THEN TRUE ELSE FALSE END AS by_week_6
    , CASE WHEN q.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 49 DAY) THEN TRUE ELSE FALSE END AS by_week_7
    , CASE WHEN q.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 56 DAY) THEN TRUE ELSE FALSE END AS by_week_8
    , CASE WHEN q.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 63 DAY) THEN TRUE ELSE FALSE END AS by_week_9
    , CASE WHEN q.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 70 DAY) THEN TRUE ELSE FALSE END AS by_week_10
    , CASE WHEN q.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 77 DAY) THEN TRUE ELSE FALSE END AS by_week_11
    , CASE WHEN q.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 84 DAY) THEN TRUE ELSE FALSE END AS by_week_12
    , CASE WHEN q.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 91 DAY) THEN TRUE ELSE FALSE END AS by_week_13
    , CASE WHEN q.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 98 DAY) THEN TRUE ELSE FALSE END AS by_week_14
    , CASE WHEN q.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 105 DAY) THEN TRUE ELSE FALSE END AS by_week_15
    , CASE WHEN q.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 112 DAY) THEN TRUE ELSE FALSE END AS by_week_16
    , CASE WHEN q.due_date >  DATETIME_ADD(cs.instruction_begin_date, INTERVAL 112 DAY) THEN TRUE ELSE FALSE END AS beyond_week_16

  FROM
    cgr_disaggregated.quiz_result qr

  INNER JOIN
    cgr_disaggregated.course_section AS cs ON qr.key = cs.key

  INNER JOIN
    cgr_disaggregated.quiz q ON qr.quiz_id = q.quiz_lms_int_id

)

SELECT
  qr.person_id AS person_id
  , qr.key AS key

  /* Total scores, by week, cumulative */
  , SUM(CASE WHEN qr.by_week_1 IS TRUE THEN qr.kept_score ELSE 0 END) AS total_score_by_week_1
  , SUM(CASE WHEN qr.by_week_2 IS TRUE THEN qr.kept_score ELSE 0 END) AS total_score_by_week_2
  , SUM(CASE WHEN qr.by_week_3 IS TRUE THEN qr.kept_score ELSE 0 END) AS total_score_by_week_3
  , SUM(CASE WHEN qr.by_week_4 IS TRUE THEN qr.kept_score ELSE 0 END) AS total_score_by_week_4
  , SUM(CASE WHEN qr.by_week_5 IS TRUE THEN qr.kept_score ELSE 0 END) AS total_score_by_week_5
  , SUM(CASE WHEN qr.by_week_6 IS TRUE THEN qr.kept_score ELSE 0 END) AS total_score_by_week_6
  , SUM(CASE WHEN qr.by_week_7 IS TRUE THEN qr.kept_score ELSE 0 END) AS total_score_by_week_7
  , SUM(CASE WHEN qr.by_week_8 IS TRUE THEN qr.kept_score ELSE 0 END) AS total_score_by_week_8
  , SUM(CASE WHEN qr.by_week_9 IS TRUE THEN qr.kept_score ELSE 0 END) AS total_score_by_week_9
  , SUM(CASE WHEN qr.by_week_10 IS TRUE THEN qr.kept_score ELSE 0 END) AS total_score_by_week_10
  , SUM(CASE WHEN qr.by_week_11 IS TRUE THEN qr.kept_score ELSE 0 END) AS total_score_by_week_11
  , SUM(CASE WHEN qr.by_week_12 IS TRUE THEN qr.kept_score ELSE 0 END) AS total_score_by_week_12
  , SUM(CASE WHEN qr.by_week_13 IS TRUE THEN qr.kept_score ELSE 0 END) AS total_score_by_week_13
  , SUM(CASE WHEN qr.by_week_14 IS TRUE THEN qr.kept_score ELSE 0 END) AS total_score_by_week_14
  , SUM(CASE WHEN qr.by_week_15 IS TRUE THEN qr.kept_score ELSE 0 END) AS total_score_by_week_15
  , SUM(CASE WHEN qr.by_week_16 IS TRUE THEN qr.kept_score ELSE 0 END) AS total_score_by_week_16
  , SUM(CASE WHEN qr.beyond_week_16 IS TRUE THEN qr.kept_score ELSE 0 END) AS total_score_beyond_week_16
  , SUM(qr.kept_score) total_score_for_course

  /* Total points possible, by week, cumulative */
  , SUM(CASE WHEN qr.by_week_1 IS TRUE THEN qr.points_possible ELSE 0 END) AS total_points_possible_by_week_1
  , SUM(CASE WHEN qr.by_week_2 IS TRUE THEN qr.points_possible ELSE 0 END) AS total_points_possible_by_week_2
  , SUM(CASE WHEN qr.by_week_3 IS TRUE THEN qr.points_possible ELSE 0 END) AS total_points_possible_by_week_3
  , SUM(CASE WHEN qr.by_week_4 IS TRUE THEN qr.points_possible ELSE 0 END) AS total_points_possible_by_week_4
  , SUM(CASE WHEN qr.by_week_5 IS TRUE THEN qr.points_possible ELSE 0 END) AS total_points_possible_by_week_5
  , SUM(CASE WHEN qr.by_week_6 IS TRUE THEN qr.points_possible ELSE 0 END) AS total_points_possible_by_week_6
  , SUM(CASE WHEN qr.by_week_7 IS TRUE THEN qr.points_possible ELSE 0 END) AS total_points_possible_by_week_7
  , SUM(CASE WHEN qr.by_week_8 IS TRUE THEN qr.points_possible ELSE 0 END) AS total_points_possible_by_week_8
  , SUM(CASE WHEN qr.by_week_9 IS TRUE THEN qr.points_possible ELSE 0 END) AS total_points_possible_by_week_9
  , SUM(CASE WHEN qr.by_week_10 IS TRUE THEN qr.points_possible ELSE 0 END) AS total_points_possible_by_week_10
  , SUM(CASE WHEN qr.by_week_11 IS TRUE THEN qr.points_possible ELSE 0 END) AS total_points_possible_by_week_11
  , SUM(CASE WHEN qr.by_week_12 IS TRUE THEN qr.points_possible ELSE 0 END) AS total_points_possible_by_week_12
  , SUM(CASE WHEN qr.by_week_13 IS TRUE THEN qr.points_possible ELSE 0 END) AS total_points_possible_by_week_13
  , SUM(CASE WHEN qr.by_week_14 IS TRUE THEN qr.points_possible ELSE 0 END) AS total_points_possible_by_week_14
  , SUM(CASE WHEN qr.by_week_15 IS TRUE THEN qr.points_possible ELSE 0 END) AS total_points_possible_by_week_15
  , SUM(CASE WHEN qr.by_week_16 IS TRUE THEN qr.points_possible ELSE 0 END) AS total_points_possible_by_week_16
  , SUM(CASE WHEN qr.beyond_week_16 IS TRUE THEN qr.points_possible ELSE 0 END) AS total_points_possible_beyond_week_16
  , SUM(qr.points_possible) total_points_possible_for_course

  /* Average score */
  , CASE
    WHEN
      SUM(CASE WHEN qr.by_week_1 IS TRUE THEN qr.points_possible ELSE 0 END) IS NOT NULL AND
      SUM(CASE WHEN qr.by_week_1 IS TRUE THEN qr.points_possible ELSE 0 END) > 0
    THEN
      SUM(CASE WHEN qr.by_week_1 IS TRUE THEN qr.kept_score ELSE 0 END) /
      SUM(CASE WHEN qr.by_week_1 IS TRUE THEN qr.points_possible ELSE 0 END)
    ELSE NULL
    END AS avg_score_by_week_1

  , CASE
    WHEN
      SUM(CASE WHEN qr.by_week_2 IS TRUE THEN qr.points_possible ELSE 0 END) IS NOT NULL AND
      SUM(CASE WHEN qr.by_week_2 IS TRUE THEN qr.points_possible ELSE 0 END) > 0
    THEN
      SUM(CASE WHEN qr.by_week_2 IS TRUE THEN qr.kept_score ELSE 0 END) /
      SUM(CASE WHEN qr.by_week_2 IS TRUE THEN qr.points_possible ELSE 0 END)
    ELSE NULL
    END AS avg_score_by_week_2

  , CASE
    WHEN
      SUM(CASE WHEN qr.by_week_3 IS TRUE THEN qr.points_possible ELSE 0 END) IS NOT NULL AND
      SUM(CASE WHEN qr.by_week_3 IS TRUE THEN qr.points_possible ELSE 0 END) > 0
    THEN
      SUM(CASE WHEN qr.by_week_3 IS TRUE THEN qr.kept_score ELSE 0 END) /
      SUM(CASE WHEN qr.by_week_3 IS TRUE THEN qr.points_possible ELSE 0 END)
    ELSE NULL
    END AS avg_score_by_week_3

  , CASE
    WHEN
      SUM(CASE WHEN qr.by_week_4 IS TRUE THEN qr.points_possible ELSE 0 END) IS NOT NULL AND
      SUM(CASE WHEN qr.by_week_4 IS TRUE THEN qr.points_possible ELSE 0 END) > 0
    THEN
      SUM(CASE WHEN qr.by_week_4 IS TRUE THEN qr.kept_score ELSE 0 END) /
      SUM(CASE WHEN qr.by_week_4 IS TRUE THEN qr.points_possible ELSE 0 END)
    ELSE NULL
    END AS avg_score_by_week_4

  , CASE
    WHEN
      SUM(CASE WHEN qr.by_week_5 IS TRUE THEN qr.points_possible ELSE 0 END) IS NOT NULL AND
      SUM(CASE WHEN qr.by_week_5 IS TRUE THEN qr.points_possible ELSE 0 END) > 0
    THEN
      SUM(CASE WHEN qr.by_week_5 IS TRUE THEN qr.kept_score ELSE 0 END) /
      SUM(CASE WHEN qr.by_week_5 IS TRUE THEN qr.points_possible ELSE 0 END)
    ELSE NULL
    END AS avg_score_by_week_5

  , CASE
    WHEN
      SUM(CASE WHEN qr.by_week_6 IS TRUE THEN qr.points_possible ELSE 0 END) IS NOT NULL AND
      SUM(CASE WHEN qr.by_week_6 IS TRUE THEN qr.points_possible ELSE 0 END) > 0
    THEN
      SUM(CASE WHEN qr.by_week_6 IS TRUE THEN qr.kept_score ELSE 0 END) /
      SUM(CASE WHEN qr.by_week_6 IS TRUE THEN qr.points_possible ELSE 0 END)
    ELSE NULL
    END AS avg_score_by_week_6

  , CASE
    WHEN
      SUM(CASE WHEN qr.by_week_7 IS TRUE THEN qr.points_possible ELSE 0 END) IS NOT NULL AND
      SUM(CASE WHEN qr.by_week_7 IS TRUE THEN qr.points_possible ELSE 0 END) > 0
    THEN
      SUM(CASE WHEN qr.by_week_7 IS TRUE THEN qr.kept_score ELSE 0 END) /
      SUM(CASE WHEN qr.by_week_7 IS TRUE THEN qr.points_possible ELSE 0 END)
    ELSE NULL
    END AS avg_score_by_week_7

  , CASE
    WHEN
      SUM(CASE WHEN qr.by_week_8 IS TRUE THEN qr.points_possible ELSE 0 END) IS NOT NULL AND
      SUM(CASE WHEN qr.by_week_8 IS TRUE THEN qr.points_possible ELSE 0 END) > 0
    THEN
      SUM(CASE WHEN qr.by_week_8 IS TRUE THEN qr.kept_score ELSE 0 END) /
      SUM(CASE WHEN qr.by_week_8 IS TRUE THEN qr.points_possible ELSE 0 END)
    ELSE NULL
    END AS avg_score_by_week_8

  , CASE
    WHEN
      SUM(CASE WHEN qr.by_week_9 IS TRUE THEN qr.points_possible ELSE 0 END) IS NOT NULL AND
      SUM(CASE WHEN qr.by_week_9 IS TRUE THEN qr.points_possible ELSE 0 END) > 0
    THEN
      SUM(CASE WHEN qr.by_week_9 IS TRUE THEN qr.kept_score ELSE 0 END) /
      SUM(CASE WHEN qr.by_week_9 IS TRUE THEN qr.points_possible ELSE 0 END)
    ELSE NULL
    END AS avg_score_by_week_9

  , CASE
    WHEN
      SUM(CASE WHEN qr.by_week_10 IS TRUE THEN qr.points_possible ELSE 0 END) IS NOT NULL AND
      SUM(CASE WHEN qr.by_week_10 IS TRUE THEN qr.points_possible ELSE 0 END) > 0
    THEN
      SUM(CASE WHEN qr.by_week_10 IS TRUE THEN qr.kept_score ELSE 0 END) /
      SUM(CASE WHEN qr.by_week_10 IS TRUE THEN qr.points_possible ELSE 0 END)
    ELSE NULL
    END AS avg_score_by_week_10

  , CASE
    WHEN
      SUM(CASE WHEN qr.by_week_11 IS TRUE THEN qr.points_possible ELSE 0 END) IS NOT NULL AND
      SUM(CASE WHEN qr.by_week_11 IS TRUE THEN qr.points_possible ELSE 0 END) > 0
    THEN
      SUM(CASE WHEN qr.by_week_11 IS TRUE THEN qr.kept_score ELSE 0 END) /
      SUM(CASE WHEN qr.by_week_11 IS TRUE THEN qr.points_possible ELSE 0 END)
    ELSE NULL
    END AS avg_score_by_week_11

  , CASE
    WHEN
      SUM(CASE WHEN qr.by_week_12 IS TRUE THEN qr.points_possible ELSE 0 END) IS NOT NULL AND
      SUM(CASE WHEN qr.by_week_12 IS TRUE THEN qr.points_possible ELSE 0 END) > 0
    THEN
      SUM(CASE WHEN qr.by_week_12 IS TRUE THEN qr.kept_score ELSE 0 END) /
      SUM(CASE WHEN qr.by_week_12 IS TRUE THEN qr.points_possible ELSE 0 END)
    ELSE NULL
    END AS avg_score_by_week_12

  , CASE
    WHEN
      SUM(CASE WHEN qr.by_week_13 IS TRUE THEN qr.points_possible ELSE 0 END) IS NOT NULL AND
      SUM(CASE WHEN qr.by_week_13 IS TRUE THEN qr.points_possible ELSE 0 END) > 0
    THEN
      SUM(CASE WHEN qr.by_week_13 IS TRUE THEN qr.kept_score ELSE 0 END) /
      SUM(CASE WHEN qr.by_week_13 IS TRUE THEN qr.points_possible ELSE 0 END)
    ELSE NULL
    END AS avg_score_by_week_13

  , CASE
    WHEN
      SUM(CASE WHEN qr.by_week_14 IS TRUE THEN qr.points_possible ELSE 0 END) IS NOT NULL AND
      SUM(CASE WHEN qr.by_week_14 IS TRUE THEN qr.points_possible ELSE 0 END) > 0
    THEN
      SUM(CASE WHEN qr.by_week_14 IS TRUE THEN qr.kept_score ELSE 0 END) /
      SUM(CASE WHEN qr.by_week_14 IS TRUE THEN qr.points_possible ELSE 0 END)
    ELSE NULL
    END AS avg_score_by_week_14

  , CASE
    WHEN
      SUM(CASE WHEN qr.by_week_15 IS TRUE THEN qr.points_possible ELSE 0 END) IS NOT NULL AND
      SUM(CASE WHEN qr.by_week_15 IS TRUE THEN qr.points_possible ELSE 0 END) > 0
    THEN
      SUM(CASE WHEN qr.by_week_15 IS TRUE THEN qr.kept_score ELSE 0 END) /
      SUM(CASE WHEN qr.by_week_15 IS TRUE THEN qr.points_possible ELSE 0 END)
    ELSE NULL
    END AS avg_score_by_week_15

  , CASE
    WHEN
      SUM(CASE WHEN qr.by_week_16 IS TRUE THEN qr.points_possible ELSE 0 END) IS NOT NULL AND
      SUM(CASE WHEN qr.by_week_16 IS TRUE THEN qr.points_possible ELSE 0 END) > 0
    THEN
      SUM(CASE WHEN qr.by_week_16 IS TRUE THEN qr.kept_score ELSE 0 END) /
      SUM(CASE WHEN qr.by_week_16 IS TRUE THEN qr.points_possible ELSE 0 END)
    ELSE NULL
    END AS avg_score_by_week_16

  , CASE
    WHEN
      SUM(CASE WHEN qr.beyond_week_16 IS TRUE THEN qr.points_possible ELSE 0 END) IS NOT NULL AND
      SUM(CASE WHEN qr.beyond_week_16 IS TRUE THEN qr.points_possible ELSE 0 END) > 0
    THEN
      SUM(CASE WHEN qr.beyond_week_16 IS TRUE THEN qr.kept_score ELSE 0 END) /
      SUM(CASE WHEN qr.beyond_week_16 IS TRUE THEN qr.points_possible ELSE 0 END)
    ELSE NULL
    END AS avg_score_beyond_week_16

  , CASE
    WHEN
      SUM(qr.points_possible) IS NOT NULL AND SUM(qr.points_possible) > 0
    THEN
      SUM(qr.kept_score) / SUM(qr.points_possible)
    ELSE NULL
    END AS avg_score_for_course

FROM
  quiz_results_with_due_date_in_term qr

GROUP BY
  qr.person_id
  , qr.key
;
