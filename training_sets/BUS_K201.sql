SELECT
  *
  , CASE WHEN
      grade_on_official_transcript = 'A+'   OR
      grade_on_official_transcript = 'A'   OR
      grade_on_official_transcript = 'A-'   OR
      grade_on_official_transcript = 'B+'   OR
      grade_on_official_transcript = 'B'   OR
      grade_on_official_transcript = 'B-'   OR
      grade_on_official_transcript = 'C+'   OR
      grade_on_official_transcript = 'C'   OR
      grade_on_official_transcript = 'C-'   OR
      grade_on_official_transcript = 'AUS'   OR
      grade_on_official_transcript = 'H'     OR
      grade_on_official_transcript = 'H-'    OR
      grade_on_official_transcript = 'I'    OR
      grade_on_official_transcript = 'P'   OR
      grade_on_official_transcript = 'S'
    THEN FALSE

    WHEN
      grade_on_official_transcript = 'AUU'   OR
      grade_on_official_transcript = 'D+'   OR
      grade_on_official_transcript = 'D'   OR
      grade_on_official_transcript = 'D-'   OR
      grade_on_official_transcript = 'F'   OR
      grade_on_official_transcript = 'FAD'   OR
      grade_on_official_transcript = 'IAD'   OR
      grade_on_official_transcript = 'U'   OR
      grade_on_official_transcript = 'W'
    THEN TRUE

    END is_dfw

FROM
  cgr_consolidated.final
WHERE

  /* Only students. */
  role = 'Student'

  /* Only BUS K201 */
  AND campus = 'BL'
  AND course_subject = 'BUS'
  AND course_number = 'K201'

  /* Only relevant grades */
  AND grade_on_official_transcript != 'FN'
  AND grade_on_official_transcript != 'FNN'
  AND grade_on_official_transcript != 'NG'
  AND grade_on_official_transcript != 'NGC'
  AND grade_on_official_transcript != 'NoData'
  AND grade_on_official_transcript != 'Numerical'
  AND grade_on_official_transcript != 'Other'
  AND grade_on_official_transcript != 'R'
  AND grade_on_official_transcript IS NOT NULL
 ;
