/*
  Logic to decide whether a course grade is a
  DFW or not. Values are determined by the
  Course grade option set:

  https://docs.udp.unizin.org/tables/ref_course_grade.html

*/

, CASE WHEN

    /* Non-D, F, and W grades */
    grade_on_official_transcript = 'A+'   OR
    grade_on_official_transcript = 'A'   OR
    grade_on_official_transcript = 'A-'   OR
    grade_on_official_transcript = 'B+'   OR
    grade_on_official_transcript = 'B'   OR
    grade_on_official_transcript = 'B-'   OR
    grade_on_official_transcript = 'C+'   OR
    grade_on_official_transcript = 'C'   OR
    grade_on_official_transcript = 'C-'   OR
    grade_on_official_transcript = 'AUS'   OR
    grade_on_official_transcript = 'H'     OR
    grade_on_official_transcript = 'H-'    OR
    grade_on_official_transcript = 'I'    OR
    grade_on_official_transcript = 'P'   OR
    grade_on_official_transcript = 'S'
  THEN FALSE

  /* D, F, or W grades. */
  WHEN
    grade_on_official_transcript = 'AUU'   OR
    grade_on_official_transcript = 'D+'   OR
    grade_on_official_transcript = 'D'   OR
    grade_on_official_transcript = 'D-'   OR
    grade_on_official_transcript = 'F'   OR
    grade_on_official_transcript = 'FAD'   OR
    grade_on_official_transcript = 'IAD'   OR
    grade_on_official_transcript = 'U'   OR
    grade_on_official_transcript = 'W'
  THEN TRUE

  END is_dfw


/* Where clause .... ignore these grades. */
WHERE
  grade_on_official_transcript != 'FN'   AND
  grade_on_official_transcript != 'FNN'   AND
  grade_on_official_transcript != 'NG'   AND
  grade_on_official_transcript != 'NGC'   AND
  grade_on_official_transcript != 'NoData'   AND
  grade_on_official_transcript != 'Numerical'   AND
  grade_on_official_transcript != 'Other'   AND
  grade_on_official_transcript != 'R'
  grade_on_official_transcript IS NOT NULL
