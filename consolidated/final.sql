SELECT
  -- Basic data about the person and course.
  cse.person_id
  , cse.key

  , REGEXP_EXTRACT(cse.key, r'(^\w+)-\w+-\w+-\w+-\w+') AS term
  , REGEXP_EXTRACT(cse.key, r'^\w+-(\w+)-\w+-\w+-\w+') AS campus
  , REGEXP_EXTRACT(cse.key, r'^\w+-\w+-(\w+)-\w+-\w+') AS course_subject
  , REGEXP_EXTRACT(cse.key, r'^\w+-\w+-\w+-(\w+)-\w+') AS course_number
  , REGEXP_EXTRACT(cse.key, r'^\w+-\w+-\w+-\w+-(\w+)') AS class_number

  /*
    Person data
  */
  , p.sex
  , p.gender
  , p.is_black_african_american
  , p.is_american_indian_alaska_native
  , p.is_asian
  , p.is_hawaiian_pacific_islander
  , p.is_hispanic_latino
  , p.is_white
  , p.is_other_american
  , p.is_no_race_indicated
  , p.is_first_generation_hed_student
  , p.is_english_first_language
  , p.was_veteran_at_admissions
  , p.zip_code_first_us_permanent_residence
  , p.zip_code_at_time_of_application
  , p.neighborhood_cb_cluster_code_at_time_of_application
  , p.estimated_gross_family_income
  , p.hs_cb_cluster_code
  , p.hs_ceeb_code
  , p.hs_zip_code
  , p.hs_state_code
  , p.hs_city_name
  , p.hs_gpa
  , CAST(p.hs_percentile_rank AS NUMERIC) / 100 AS hs_percentile_rank

  /*
    Data about the student's enrollment
    in this course.
  */
  , cse.role
  , cse.credits_taken
  , cse.credits_earned

  /*
    Course grade earned
  */
  , cg.grade_inputted_by_instructor
  , cg.grade_on_official_transcript
  , cg.grade_points_per_credit
  , cg.grade_points
  , cg.is_course_grade_in_gpa
  , cg.gpa_cumulative_excluding_course_grade
  , cg.gpa_current_excluding_course_grade
  , cg.grading_basis

  /*
    Course level learning data.
  */
  , cm.total_module_items
  , cm.total_modules
  , cm.avg_module_items_per_module

  /*
    Course learner activity scores
  */
  , clar.avg_score_by_week_1 AS avg_course_learner_activity_score_by_week_1
  , clar.avg_score_by_week_2 AS avg_course_learner_activity_score_by_week_2
  , clar.avg_score_by_week_3 AS avg_course_learner_activity_score_by_week_3
  , clar.avg_score_by_week_4 AS avg_course_learner_activity_score_by_week_4
  , clar.avg_score_by_week_5 AS avg_course_learner_activity_score_by_week_5
  , clar.avg_score_by_week_6 AS avg_course_learner_activity_score_by_week_6
  , clar.avg_score_by_week_7 AS avg_course_learner_activity_score_by_week_7
  , clar.avg_score_by_week_8 AS avg_course_learner_activity_score_by_week_8
  , clar.avg_score_by_week_9 AS avg_course_learner_activity_score_by_week_9
  , clar.avg_score_by_week_10 AS avg_course_learner_activity_score_by_week_10
  , clar.avg_score_by_week_11 AS avg_course_learner_activity_score_by_week_11
  , clar.avg_score_by_week_12 AS avg_course_learner_activity_score_by_week_12
  , clar.avg_score_by_week_13 AS avg_course_learner_activity_score_by_week_13
  , clar.avg_score_by_week_14 AS avg_course_learner_activity_score_by_week_14
  , clar.avg_score_by_week_15 AS avg_course_learner_activity_score_by_week_15
  , clar.avg_score_by_week_16 AS avg_course_learner_activity_score_by_week_16
  , clar.avg_score_beyond_week_16 AS avg_course_learner_activity_score_beyond_week_16
  , clar.avg_score_for_course AS avg_course_learner_activity_score

  /*
    Course quiz scores
  */
  , cqr.avg_score_by_week_1 AS avg_course_quiz_score_by_week_1
  , cqr.avg_score_by_week_2 AS avg_course_quiz_score_by_week_2
  , cqr.avg_score_by_week_3 AS avg_course_quiz_score_by_week_3
  , cqr.avg_score_by_week_4 AS avg_course_quiz_score_by_week_4
  , cqr.avg_score_by_week_5 AS avg_course_quiz_score_by_week_5
  , cqr.avg_score_by_week_6 AS avg_course_quiz_score_by_week_6
  , cqr.avg_score_by_week_7 AS avg_course_quiz_score_by_week_7
  , cqr.avg_score_by_week_8 AS avg_course_quiz_score_by_week_8
  , cqr.avg_score_by_week_9 AS avg_course_quiz_score_by_week_9
  , cqr.avg_score_by_week_10 AS avg_course_quiz_score_by_week_10
  , cqr.avg_score_by_week_11 AS avg_course_quiz_score_by_week_11
  , cqr.avg_score_by_week_12 AS avg_course_quiz_score_by_week_12
  , cqr.avg_score_by_week_13 AS avg_course_quiz_score_by_week_13
  , cqr.avg_score_by_week_14 AS avg_course_quiz_score_by_week_14
  , cqr.avg_score_by_week_15 AS avg_course_quiz_score_by_week_15
  , cqr.avg_score_by_week_16 AS avg_course_quiz_score_by_week_16
  , cqr.avg_score_beyond_week_16 AS avg_course_quiz_score_beyond_week_16
  , cqr.avg_score_for_course AS avg_course_quiz_score

  /*
    Discussion entries
  */
  , cde.avg_entries_per_student_by_week_1 AS avg_entries_per_student_by_week_1
  , cde.avg_entries_per_student_by_week_2 AS avg_entries_per_student_by_week_2
  , cde.avg_entries_per_student_by_week_3 AS avg_entries_per_student_by_week_3
  , cde.avg_entries_per_student_by_week_4 AS avg_entries_per_student_by_week_4
  , cde.avg_entries_per_student_by_week_5 AS avg_entries_per_student_by_week_5
  , cde.avg_entries_per_student_by_week_6 AS avg_entries_per_student_by_week_6
  , cde.avg_entries_per_student_by_week_7 AS avg_entries_per_student_by_week_7
  , cde.avg_entries_per_student_by_week_8 AS avg_entries_per_student_by_week_8
  , cde.avg_entries_per_student_by_week_9 AS avg_entries_per_student_by_week_9
  , cde.avg_entries_per_student_by_week_10 AS avg_entries_per_student_by_week_10
  , cde.avg_entries_per_student_by_week_11 AS avg_entries_per_student_by_week_11
  , cde.avg_entries_per_student_by_week_12 AS avg_entries_per_student_by_week_12
  , cde.avg_entries_per_student_by_week_13 AS avg_entries_per_student_by_week_13
  , cde.avg_entries_per_student_by_week_14 AS avg_entries_per_student_by_week_14
  , cde.avg_entries_per_student_by_week_15 AS avg_entries_per_student_by_week_15
  , cde.avg_entries_per_student_by_week_16 AS avg_entries_per_student_by_week_16
  , cde.avg_entries_per_student_beyond_week_16 AS avg_entries_per_student_beyond_week_16
  , cde.avg_entries_per_student AS avg_entries_per_student

  /*
    Student learner activity scores
  */
  , plar.avg_score_by_week_1 AS avg_student_learner_activity_score_by_week_1
  , plar.avg_score_by_week_2 AS avg_student_learner_activity_score_by_week_2
  , plar.avg_score_by_week_3 AS avg_student_learner_activity_score_by_week_3
  , plar.avg_score_by_week_4 AS avg_student_learner_activity_score_by_week_4
  , plar.avg_score_by_week_5 AS avg_student_learner_activity_score_by_week_5
  , plar.avg_score_by_week_6 AS avg_student_learner_activity_score_by_week_6
  , plar.avg_score_by_week_7 AS avg_student_learner_activity_score_by_week_7
  , plar.avg_score_by_week_8 AS avg_student_learner_activity_score_by_week_8
  , plar.avg_score_by_week_9 AS avg_student_learner_activity_score_by_week_9
  , plar.avg_score_by_week_10 AS avg_student_learner_activity_score_by_week_10
  , plar.avg_score_by_week_11 AS avg_student_learner_activity_score_by_week_11
  , plar.avg_score_by_week_12 AS avg_student_learner_activity_score_by_week_12
  , plar.avg_score_by_week_13 AS avg_student_learner_activity_score_by_week_13
  , plar.avg_score_by_week_14 AS avg_student_learner_activity_score_by_week_14
  , plar.avg_score_by_week_15 AS avg_student_learner_activity_score_by_week_15
  , plar.avg_score_by_week_16 AS avg_student_learner_activity_score_by_week_16
  , plar.avg_score_beyond_week_16 AS avg_student_learner_activity_score_beyond_week_16
  , plar.avg_score_for_course AS avg_student_learner_activity_score

  /*
    Student quiz scores
  */
  , pqr.avg_score_by_week_1 AS avg_student_quiz_score_by_week_1
  , pqr.avg_score_by_week_2 AS avg_student_quiz_score_by_week_2
  , pqr.avg_score_by_week_3 AS avg_student_quiz_score_by_week_3
  , pqr.avg_score_by_week_4 AS avg_student_quiz_score_by_week_4
  , pqr.avg_score_by_week_5 AS avg_student_quiz_score_by_week_5
  , pqr.avg_score_by_week_6 AS avg_student_quiz_score_by_week_6
  , pqr.avg_score_by_week_7 AS avg_student_quiz_score_by_week_7
  , pqr.avg_score_by_week_8 AS avg_student_quiz_score_by_week_8
  , pqr.avg_score_by_week_9 AS avg_student_quiz_score_by_week_9
  , pqr.avg_score_by_week_10 AS avg_student_quiz_score_by_week_10
  , pqr.avg_score_by_week_11 AS avg_student_quiz_score_by_week_11
  , pqr.avg_score_by_week_12 AS avg_student_quiz_score_by_week_12
  , pqr.avg_score_by_week_13 AS avg_student_quiz_score_by_week_13
  , pqr.avg_score_by_week_14 AS avg_student_quiz_score_by_week_14
  , pqr.avg_score_by_week_15 AS avg_student_quiz_score_by_week_15
  , pqr.avg_score_by_week_16 AS avg_student_quiz_score_by_week_16
  , pqr.avg_score_beyond_week_16 AS avg_student_quiz_score_beyond_week_16
  , pqr.avg_score_for_course AS avg_student_quiz_score

  /*
    Student entries
  */
  , pde.total_entries_by_week_1 AS total_student_entries_by_week_1
  , pde.total_entries_by_week_2 AS total_student_entries_by_week_2
  , pde.total_entries_by_week_3 AS total_student_entries_by_week_3
  , pde.total_entries_by_week_4 AS total_student_entries_by_week_4
  , pde.total_entries_by_week_5 AS total_student_entries_by_week_5
  , pde.total_entries_by_week_6 AS total_student_entries_by_week_6
  , pde.total_entries_by_week_7 AS total_student_entries_by_week_7
  , pde.total_entries_by_week_8 AS total_student_entries_by_week_8
  , pde.total_entries_by_week_9 AS total_student_entries_by_week_9
  , pde.total_entries_by_week_10 AS total_student_entries_by_week_10
  , pde.total_entries_by_week_11 AS total_student_entries_by_week_11
  , pde.total_entries_by_week_12 AS total_student_entries_by_week_12
  , pde.total_entries_by_week_13 AS total_student_entries_by_week_13
  , pde.total_entries_by_week_14 AS total_student_entries_by_week_14
  , pde.total_entries_by_week_15 AS total_student_entries_by_week_15
  , pde.total_entries_by_week_16 AS total_student_entries_by_week_16
  , pde.total_entries_beyond_week_16 AS total_student_entries_beyond_week_16
  , pde.total_entries AS total_entries_by_week_1

  /*
    Person/Course section relative score, learner activity
  */
  , CASE
    WHEN clar.avg_score_by_week_1 IS NOT NULL
    THEN COALESCE(plar.avg_score_by_week_1, 0) - COALESCE(clar.avg_score_by_week_1, 0)
    ELSE NULL
    END AS student_la_score_ahead_behind_course_by_week_1

  , CASE
    WHEN clar.avg_score_by_week_2 IS NOT NULL
    THEN COALESCE(plar.avg_score_by_week_2, 0) - COALESCE(clar.avg_score_by_week_2, 0)
    ELSE NULL
    END AS student_la_score_ahead_behind_course_by_week_2

  , CASE
    WHEN clar.avg_score_by_week_3 IS NOT NULL
    THEN COALESCE(plar.avg_score_by_week_3, 0) - COALESCE(clar.avg_score_by_week_3, 0)
    ELSE NULL
    END AS student_la_score_ahead_behind_course_by_week_3

  , CASE
    WHEN clar.avg_score_by_week_4 IS NOT NULL
    THEN COALESCE(plar.avg_score_by_week_4, 0) - COALESCE(clar.avg_score_by_week_4, 0)
    ELSE NULL
    END AS student_la_score_ahead_behind_course_by_week_4

  , CASE
    WHEN clar.avg_score_by_week_5 IS NOT NULL
    THEN COALESCE(plar.avg_score_by_week_5, 0) - COALESCE(clar.avg_score_by_week_5, 0)
    ELSE NULL
    END AS student_la_score_ahead_behind_course_by_week_5

  , CASE
    WHEN clar.avg_score_by_week_6 IS NOT NULL
    THEN COALESCE(plar.avg_score_by_week_6, 0) - COALESCE(clar.avg_score_by_week_6, 0)
    ELSE NULL
    END AS student_la_score_ahead_behind_course_by_week_6

  , CASE
    WHEN clar.avg_score_by_week_7 IS NOT NULL
    THEN COALESCE(plar.avg_score_by_week_7, 0) - COALESCE(clar.avg_score_by_week_7, 0)
    ELSE NULL
    END AS student_la_score_ahead_behind_course_by_week_7

  , CASE
    WHEN clar.avg_score_by_week_8 IS NOT NULL
    THEN COALESCE(plar.avg_score_by_week_8, 0) - COALESCE(clar.avg_score_by_week_8, 0)
    ELSE NULL
    END AS student_la_score_ahead_behind_course_by_week_8

  , CASE
    WHEN clar.avg_score_by_week_9 IS NOT NULL
    THEN COALESCE(plar.avg_score_by_week_9, 0) - COALESCE(clar.avg_score_by_week_9, 0)
    ELSE NULL
    END AS student_la_score_ahead_behind_course_by_week_9

  , CASE
    WHEN clar.avg_score_by_week_10 IS NOT NULL
    THEN COALESCE(plar.avg_score_by_week_10, 0) - COALESCE(clar.avg_score_by_week_10, 0)
    ELSE NULL
    END AS student_la_score_ahead_behind_course_by_week_10

  , CASE
    WHEN clar.avg_score_by_week_11 IS NOT NULL
    THEN COALESCE(plar.avg_score_by_week_11, 0) - COALESCE(clar.avg_score_by_week_11, 0)
    ELSE NULL
    END AS student_la_score_ahead_behind_course_by_week_11

  , CASE
    WHEN clar.avg_score_by_week_12 IS NOT NULL
    THEN COALESCE(plar.avg_score_by_week_12, 0) - COALESCE(clar.avg_score_by_week_12, 0)
    ELSE NULL
    END AS student_la_score_ahead_behind_course_by_week_12

  , CASE
    WHEN clar.avg_score_by_week_13 IS NOT NULL
    THEN COALESCE(plar.avg_score_by_week_13, 0) - COALESCE(clar.avg_score_by_week_13, 0)
    ELSE NULL
    END AS student_la_score_ahead_behind_course_by_week_13

  , CASE
    WHEN clar.avg_score_by_week_14 IS NOT NULL
    THEN COALESCE(plar.avg_score_by_week_14, 0) - COALESCE(clar.avg_score_by_week_4, 0)
    ELSE NULL
    END AS student_la_score_ahead_behind_course_by_week_14

  , CASE
    WHEN clar.avg_score_by_week_15 IS NOT NULL
    THEN COALESCE(plar.avg_score_by_week_15, 0) - COALESCE(clar.avg_score_by_week_15, 0)
    ELSE NULL
    END AS student_la_score_ahead_behind_course_by_week_15

  , CASE
    WHEN clar.avg_score_by_week_16 IS NOT NULL
    THEN COALESCE(plar.avg_score_by_week_16, 0) - COALESCE(clar.avg_score_by_week_16, 0)
    ELSE NULL
    END AS student_la_score_ahead_behind_course_by_week_16

  , CASE
    WHEN clar.avg_score_beyond_week_16 IS NOT NULL
    THEN COALESCE(plar.avg_score_beyond_week_16, 0) - COALESCE(clar.avg_score_beyond_week_16, 0)
    ELSE NULL
    END AS student_la_score_ahead_behind_course_avg_score_beyond_week_16

  , CASE
    WHEN clar.avg_score_for_course IS NOT NULL
    THEN COALESCE(plar.avg_score_for_course, 0) - COALESCE(clar.avg_score_for_course, 0)
    ELSE NULL
    END AS student_la_score_ahead_behind_course_avg_score_for_course


  /*
    Person/Course section relative score, quizzes
  */
  , CASE
    WHEN cqr.avg_score_by_week_1 IS NOT NULL
    THEN COALESCE(pqr.avg_score_by_week_1, 0) - COALESCE(cqr.avg_score_by_week_1, 0)
    ELSE NULL
    END AS student_quiz_score_ahead_behind_course_by_week_1

  , CASE
    WHEN cqr.avg_score_by_week_2 IS NOT NULL
    THEN COALESCE(pqr.avg_score_by_week_2, 0) - COALESCE(cqr.avg_score_by_week_2, 0)
    ELSE NULL
    END AS student_quiz_score_ahead_behind_course_by_week_2

  , CASE
    WHEN cqr.avg_score_by_week_3 IS NOT NULL
    THEN COALESCE(pqr.avg_score_by_week_3, 0) - COALESCE(cqr.avg_score_by_week_3, 0)
    ELSE NULL
    END AS student_quiz_score_ahead_behind_course_by_week_3

  , CASE
    WHEN cqr.avg_score_by_week_4 IS NOT NULL
    THEN COALESCE(pqr.avg_score_by_week_4, 0) - COALESCE(cqr.avg_score_by_week_4, 0)
    ELSE NULL
    END AS student_quiz_score_ahead_behind_course_by_week_4

  , CASE
    WHEN cqr.avg_score_by_week_5 IS NOT NULL
    THEN COALESCE(pqr.avg_score_by_week_5, 0) - COALESCE(cqr.avg_score_by_week_5, 0)
    ELSE NULL
    END AS student_quiz_score_ahead_behind_course_by_week_5

  , CASE
    WHEN cqr.avg_score_by_week_6 IS NOT NULL
    THEN COALESCE(pqr.avg_score_by_week_6, 0) - COALESCE(cqr.avg_score_by_week_6, 0)
    ELSE NULL
    END AS student_quiz_score_ahead_behind_course_by_week_6

  , CASE
    WHEN cqr.avg_score_by_week_7 IS NOT NULL
    THEN COALESCE(pqr.avg_score_by_week_7, 0) - COALESCE(cqr.avg_score_by_week_7, 0)
    ELSE NULL
    END AS student_quiz_score_ahead_behind_course_by_week_7

  , CASE
    WHEN cqr.avg_score_by_week_8 IS NOT NULL
    THEN COALESCE(pqr.avg_score_by_week_8, 0) - COALESCE(cqr.avg_score_by_week_8, 0)
    ELSE NULL
    END AS student_quiz_score_ahead_behind_course_by_week_8

  , CASE
    WHEN cqr.avg_score_by_week_9 IS NOT NULL
    THEN COALESCE(pqr.avg_score_by_week_9, 0) - COALESCE(cqr.avg_score_by_week_9, 0)
    ELSE NULL
    END AS student_quiz_score_ahead_behind_course_by_week_9

  , CASE
    WHEN cqr.avg_score_by_week_10 IS NOT NULL
    THEN COALESCE(pqr.avg_score_by_week_10, 0) - COALESCE(cqr.avg_score_by_week_10, 0)
    ELSE NULL
    END AS student_quiz_score_ahead_behind_course_by_week_10

  , CASE
    WHEN cqr.avg_score_by_week_11 IS NOT NULL
    THEN COALESCE(pqr.avg_score_by_week_11, 0) - COALESCE(cqr.avg_score_by_week_11, 0)
    ELSE NULL
    END AS student_quiz_score_ahead_behind_course_by_week_11

  , CASE
    WHEN cqr.avg_score_by_week_12 IS NOT NULL
    THEN COALESCE(pqr.avg_score_by_week_12, 0) - COALESCE(cqr.avg_score_by_week_12, 0)
    ELSE NULL
    END AS student_quiz_score_ahead_behind_course_by_week_12

  , CASE
    WHEN cqr.avg_score_by_week_13 IS NOT NULL
    THEN COALESCE(pqr.avg_score_by_week_13, 0) - COALESCE(cqr.avg_score_by_week_13, 0)
    ELSE NULL
    END AS student_quiz_score_ahead_behind_course_by_week_13

  , CASE
    WHEN cqr.avg_score_by_week_14 IS NOT NULL
    THEN COALESCE(pqr.avg_score_by_week_14, 0) - COALESCE(cqr.avg_score_by_week_4, 0)
    ELSE NULL
    END AS student_quiz_score_ahead_behind_course_by_week_14

  , CASE
    WHEN cqr.avg_score_by_week_15 IS NOT NULL
    THEN COALESCE(pqr.avg_score_by_week_15, 0) - COALESCE(cqr.avg_score_by_week_15, 0)
    ELSE NULL
    END AS student_quiz_score_ahead_behind_course_by_week_15

  , CASE
    WHEN cqr.avg_score_by_week_16 IS NOT NULL
    THEN COALESCE(pqr.avg_score_by_week_16, 0) - COALESCE(cqr.avg_score_by_week_16, 0)
    ELSE NULL
    END AS student_quiz_score_ahead_behind_course_by_week_16

  , CASE
    WHEN cqr.avg_score_beyond_week_16 IS NOT NULL
    THEN COALESCE(pqr.avg_score_beyond_week_16, 0) - COALESCE(cqr.avg_score_beyond_week_16, 0)
    ELSE NULL
    END AS student_quiz_score_ahead_behind_course_avg_score_beyond_week_16

  , CASE
    WHEN cqr.avg_score_for_course IS NOT NULL
    THEN COALESCE(pqr.avg_score_for_course, 0) - COALESCE(cqr.avg_score_for_course, 0)
    ELSE NULL
    END AS student_quiz_score_ahead_behind_course_avg_score_for_course

  /*
    Person/Course section relative score, discussions
  */
  , CASE
    WHEN cde.avg_entries_per_student_by_week_1 IS NOT NULL
    THEN COALESCE(pde.total_entries_by_week_1, 0) - COALESCE(cde.avg_entries_per_student_by_week_1, 0)
    ELSE NULL
    END AS student_discussion_entries_ahead_behind_course_by_week_1

  , CASE
    WHEN cde.avg_entries_per_student_by_week_2 IS NOT NULL
    THEN COALESCE(pde.total_entries_by_week_2, 0) - COALESCE(cde.avg_entries_per_student_by_week_2, 0)
    ELSE NULL
    END AS student_discussion_entries_ahead_behind_course_by_week_2

  , CASE
    WHEN cde.avg_entries_per_student_by_week_3 IS NOT NULL
    THEN COALESCE(pde.total_entries_by_week_3, 0) - COALESCE(cde.avg_entries_per_student_by_week_3, 0)
    ELSE NULL
    END AS student_discussion_entries_ahead_behind_course_by_week_3

  , CASE
    WHEN cde.avg_entries_per_student_by_week_4 IS NOT NULL
    THEN COALESCE(pde.total_entries_by_week_4, 0) - COALESCE(cde.avg_entries_per_student_by_week_4, 0)
    ELSE NULL
    END AS student_discussion_entries_ahead_behind_course_by_week_4

  , CASE
    WHEN cde.avg_entries_per_student_by_week_5 IS NOT NULL
    THEN COALESCE(pde.total_entries_by_week_5, 0) - COALESCE(cde.avg_entries_per_student_by_week_5, 0)
    ELSE NULL
    END AS student_discussion_entries_ahead_behind_course_by_week_5

  , CASE
    WHEN cde.avg_entries_per_student_by_week_6 IS NOT NULL
    THEN COALESCE(pde.total_entries_by_week_6, 0) - COALESCE(cde.avg_entries_per_student_by_week_6, 0)
    ELSE NULL
    END AS student_discussion_entries_ahead_behind_course_by_week_6

  , CASE
    WHEN cde.avg_entries_per_student_by_week_7 IS NOT NULL
    THEN COALESCE(pde.total_entries_by_week_7, 0) - COALESCE(cde.avg_entries_per_student_by_week_7, 0)
    ELSE NULL
    END AS student_discussion_entries_ahead_behind_course_by_week_7

  , CASE
    WHEN cde.avg_entries_per_student_by_week_8 IS NOT NULL
    THEN COALESCE(pde.total_entries_by_week_8, 0) - COALESCE(cde.avg_entries_per_student_by_week_8, 0)
    ELSE NULL
    END AS student_discussion_entries_ahead_behind_course_by_week_8

  , CASE
    WHEN cde.avg_entries_per_student_by_week_9 IS NOT NULL
    THEN COALESCE(pde.total_entries_by_week_9, 0) - COALESCE(cde.avg_entries_per_student_by_week_9, 0)
    ELSE NULL
    END AS student_discussion_entries_ahead_behind_course_by_week_9

  , CASE
    WHEN cde.avg_entries_per_student_by_week_10 IS NOT NULL
    THEN COALESCE(pde.total_entries_by_week_10, 0) - COALESCE(cde.avg_entries_per_student_by_week_10, 0)
    ELSE NULL
    END AS student_discussion_entries_ahead_behind_course_by_week_10

  , CASE
    WHEN cde.avg_entries_per_student_by_week_11 IS NOT NULL
    THEN COALESCE(pde.total_entries_by_week_11, 0) - COALESCE(cde.avg_entries_per_student_by_week_11, 0)
    ELSE NULL
    END AS student_discussion_entries_ahead_behind_course_by_week_11

  , CASE
    WHEN cde.avg_entries_per_student_by_week_12 IS NOT NULL
    THEN COALESCE(pde.total_entries_by_week_12, 0) - COALESCE(cde.avg_entries_per_student_by_week_12, 0)
    ELSE NULL
    END AS student_discussion_entries_ahead_behind_course_by_week_12

  , CASE
    WHEN cde.avg_entries_per_student_by_week_13 IS NOT NULL
    THEN COALESCE(pde.total_entries_by_week_13, 0) - COALESCE(cde.avg_entries_per_student_by_week_13, 0)
    ELSE NULL
    END AS student_discussion_entries_ahead_behind_course_by_week_13

  , CASE
    WHEN cde.avg_entries_per_student_by_week_14 IS NOT NULL
    THEN COALESCE(pde.total_entries_by_week_14, 0) - COALESCE(cde.avg_entries_per_student_by_week_14, 0)
    ELSE NULL
    END AS student_discussion_entries_ahead_behind_course_by_week_14

  , CASE
    WHEN cde.avg_entries_per_student_by_week_15 IS NOT NULL
    THEN COALESCE(pde.total_entries_by_week_15, 0) - COALESCE(cde.avg_entries_per_student_by_week_15, 0)
    ELSE NULL
    END AS student_discussion_entries_ahead_behind_course_by_week_15

  , CASE
    WHEN cde.avg_entries_per_student_by_week_16 IS NOT NULL
    THEN COALESCE(pde.total_entries_by_week_16, 0) - COALESCE(cde.avg_entries_per_student_by_week_6, 0)
    ELSE NULL
    END AS student_discussion_entries_ahead_behind_course_by_week_16

  , CASE
    WHEN cde.avg_entries_per_student_beyond_week_16 IS NOT NULL
    THEN COALESCE(pde.total_entries_beyond_week_16, 0) - COALESCE(cde.avg_entries_per_student_beyond_week_16, 0)
    ELSE NULL
    END AS student_discussion_entries_ahead_behind_course_beyond_week_16

  , CASE
    WHEN cde.avg_entries_per_student IS NOT NULL
    THEN COALESCE(pde.total_entries, 0) - COALESCE(cde.avg_entries_per_student, 0)
    ELSE NULL
    END AS student_discussion_entries_ahead_behind_course_for_course

FROM
  cgr_disaggregated.course_section_enrollment cse
INNER JOIN
  cgr_disaggregated.person_map pm ON cse.person_id = pm.sis_ext_id
INNER JOIN
  cgr_disaggregated.person p ON cse.person_id = p.person_id
LEFT JOIN
  cgr_disaggregated.course_grades cg ON cse.key = cg.key AND cse.person_id = cg.person_id
LEFT JOIN
  cgr_disaggregated.modules cm ON cse.key = cm.key
LEFT JOIN
  cgr_aggregated.by_course_section_learner_activity_result clar ON cse.key = clar.key
LEFT JOIN
  cgr_aggregated.by_course_section_quiz_result cqr ON cse.key = cqr.key
LEFT JOIN
  cgr_aggregated.by_course_section_discussion_entry cde ON cse.key = cde.key
LEFT JOIN
  cgr_aggregated.by_person_by_course_section_learner_activity_result plar ON cse.key = plar.key AND pm.lms_int_id = plar.person_id
LEFT JOIN
  cgr_aggregated.by_person_by_course_section_quiz_result pqr ON cse.key = pqr.key AND pm.lms_int_id = pqr.person_id
LEFT JOIN
  cgr_aggregated.by_person_by_course_section_discussion_entry pde ON cse.key = pde.key AND pm.lms_int_id = pde.person_id

;
