DROP TABLE IF EXISTS cgr.course_section;

create table cgr.course_section (
  key TEXT
  , instruction_begin_date TIMESTAMP
  , instruction_end_date TIMESTAMP
);

INSERT INTO cgr.course_section (
  key
  , instruction_begin_date
  , instruction_end_date
)

SELECT
  cs.sis_ext_id AS key
  , ss.instruction_begin_date AS instruction_begin_date
  , ss.instruction_begin_date AS instruction_end_date
FROM
  sis.course_section cs
INNER JOIN
  sis.course_offering co ON cs.course_offering_id = co.sis_int_id
INNER JOIN
  sis.academic_session ss ON co.academic_session_id = ss.sis_int_id
INNER JOIN
  sis.academic_term at ON ss.academic_term_id = at.sis_int_id
;

CREATE INDEX ON cgr.course_section(key);
