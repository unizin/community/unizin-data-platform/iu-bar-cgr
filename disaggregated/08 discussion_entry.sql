SELECT NOW();

DROP TABLE IF EXISTS cgr.discussion_entry;

CREATE TABLE cgr.discussion_entry (
  person_id TEXT
  , key TEXT
  , discussion_id TEXT
  , created_date TIMESTAMP
  , message_length NUMERIC
);

INSERT INTO cgr.discussion_entry (
  person_id
  , key
  , discussion_id
  , created_date
  , message_length
)

SELECT
  de.person_id AS person_id
  , d.key AS key
  , de.discussion_id AS discussion_id
  , de.created_date AS created_date
  , de.message_length AS message_length

FROM
  lms.discussion_entry AS de

INNER JOIN
  cgr.discussion AS d ON de.discussion_id = d.discussion_id

WHERE
  de.status = 'active' -- Only active discussion entries.
;

CREATE INDEX ON cgr.discussion_entry(person_id);
CREATE INDEX ON cgr.discussion_entry(key);

SELECT COUNT(1) from lms.discussion_entry;
SELECT COUNT(1) from cgr.discussion_entry;

SELECT NOW();
