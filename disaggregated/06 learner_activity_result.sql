SELECT NOW();

DROP TABLE IF EXISTS cgr.learner_activity_result;

create table cgr.learner_activity_result (
  person_id text
  , key text
  , learner_activity_id text
  , points_possible numeric
  , score numeric
  , published_score numeric
  , response_date timestamp
);

insert into cgr.learner_activity_result (
  person_id
  , key
  , learner_activity_id
  , points_possible
  , score
  , published_score
  , response_date
)

SELECT
  lar.person_id AS person_id
  , la.key AS key
  , lar.learner_activity_id AS learner_activity_id
  , la.points_possible AS points_possible
  , lar.score AS score
  , lar.published_score AS published_score
  , lar.response_date AS response_date

FROM
  lms.learner_activity_result AS lar

INNER JOIN
  cgr.learner_activity AS la ON lar.learner_activity_id = la.learner_activity_id

WHERE
  lar.gradebook_status = 'true' -- Use the value used in the grade book
  AND lar.grade_state != 'not_graded' -- Anything but an LAR that is not graded
;

CREATE INDEX ON cgr.learner_activity_result(person_id);
CREATE INDEX ON cgr.learner_activity_result(key);

SELECT COUNT(1) from lms.learner_activity_result;
SELECT COUNT(1) from cgr.learner_activity_result;

SELECT NOW();
