SELECT NOW();

DROP TABLE IF EXISTS cgr.quiz_result;

create table cgr.quiz_result (
  person_id text
  , key text
  , quiz_id text
  , points_possible numeric
  , score numeric
  , kept_score numeric
  , started_date timestamp
  , result_date timestamp
  , finished_date timestamp

);

insert into cgr.quiz_result (
  person_id
  , key
  , quiz_id
  , points_possible
  , score
  , kept_score
  , started_date
  , result_date
  , finished_date
)

select
  qr.person_id AS person_id
  , q.key AS key
  , qr.quiz_id AS quiz_id
  , q.points_possible AS points_possible
  , qr.score AS score
  , qr.kept_score AS kept_score
  , qr.started_date AS started_date
  , qr.result_date AS result_date
  , qr.finished_date AS finished_date

FROM
  lms.quiz_result AS qr

INNER JOIN
  cgr.quiz AS q ON qr.quiz_id = q.quiz_id

WHERE
  qr.status = 'complete' -- Only completed quiz results
;

CREATE INDEX ON cgr.quiz_result(person_id);
CREATE INDEX ON cgr.quiz_result(key);

SELECT COUNT(1) from lms.quiz;
SELECT COUNT(1) from cgr.quiz;

SELECT NOW();
