SELECT NOW();

DROP TABLE IF EXISTS cgr.discussion;

CREATE TABLE cgr.discussion (
  key TEXT -- needed to parse course data
  , discussion_id TEXT -- Needed to join discussion result data.
  , posted_date TIMESTAMP
);

INSERT INTO cgr.discussion (
  key
  , discussion_id
  , posted_date
)

SELECT
  cd.sis_source_id AS key
  , d.lms_int_id AS discussion_id
  , d.posted_date AS posted_date

FROM
  lms.discussion AS d

INNER JOIN
  cd.course_dim AS cd ON d.course_offering_id = cd.id::text

WHERE
  d.status = 'active' -- Only active discussions
  AND d.course_offering_id IS NOT NULL -- Only discussions that belong to a course.
;

CREATE INDEX ON cgr.discussion(discussion_id);

SELECT COUNT(1) from lms.discussion;
SELECT COUNT(1) from cgr.discussion;

SELECT NOW();
