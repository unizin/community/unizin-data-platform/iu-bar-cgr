DROP TABLE IF EXISTS cgr.course_section_enrollment;

create table cgr.course_section_enrollment (
  person_id TEXT
  , key TEXT
  , type TEXT
  , role TEXT
  , credits_taken NUMERIC
  , credits_earned NUMERIC
  , cen_academic_load TEXT
  , eot_academic_load TEXT
  , cen_academic_level TEXT
  , gpa_cumulative NUMERIC
  , gpa_academic_term NUMERIC
  , grade_points_earned NUMERIC
  , gpa_credits_units_hours NUMERIC
  , non_gpa_credits_units_hours INTEGER
);

INSERT INTO cgr.course_section_enrollment (
  person_id
  , key
  , type
  , role
  , credits_taken
  , credits_earned
  , cen_academic_load
  , eot_academic_load
  , cen_academic_level
  , gpa_cumulative
  , gpa_academic_term
  , grade_points_earned
  , gpa_credits_units_hours
  , non_gpa_credits_units_hours
)

SELECT
  cse.person_id
  , cs.sis_ext_id AS key
  , cs.type AS type
  , cse.role AS role
  , cse.credits_taken AS credits_taken_in_course
  , cse.credits_earned AS credits_earned_in_course
  , pat.cen_academic_load AS academic_load_at_start_of_term
  , pat.eot_academic_load AS academic_load_at_end_of_term
  , pat.cen_academic_level AS academic_level_in_term
  , pat.gpa_cumulative AS gpa_coming_into_term
  , pat.gpa_academic_term AS gpa_earned_in_term
  , pat.grade_points_earned AS grade_points_earned_in_term
  , pat.gpa_credits_units_hours AS gpa_credits_units_hours_in_term
  , pat.non_gpa_credits_units_hours AS non_gpa_credits_units_hours_in_term
FROM
  sis.course_section_enrollment cse
INNER JOIN
  sis.course_section cs ON cse.course_section_id = cs.sis_int_id
INNER JOIN
  sis.course_offering co ON cs.course_offering_id = co.sis_int_id
INNER JOIN
  sis.academic_session ss ON co.academic_session_id = ss.sis_int_id
INNER JOIN
  sis.academic_term at ON ss.academic_term_id = at.sis_int_id
LEFT JOIN
  sis.person__academic_term pat ON pat.person_id = cse.person_id AND pat.academic_term_id = at.sis_int_id
where
  cse.credits_earned IS NOT NULL
;

CREATE INDEX ON cgr.course_section_enrollment(key);
CREATE INDEX ON cgr.course_section_enrollment(person_id);
